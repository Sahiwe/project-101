package swaglabstestingwebpage;

import java.net.UnknownHostException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import checkoutInformationpage.CheckOutInformationPageAction;
import checkoutcompletpage.CheckOutCompletPageAction;
import landingPage.LandingPageAction;
import loginpage.LoginPageAction;
import mongodb.DataTable;
import overviewcheckoutpage.OverViewCheckOutPageAction;
import standardproductpage.StandardProductPageAction;
import yourcartpage.YourCartPageAction;

public class SwagLabsTestingWebPage {

	private WebDriver driver;
	private final static String projectPath = System.getProperty("user.dir");
	private String driverPath = projectPath +"\\src\\test\\resource\\webdriver\\chromedriver.exe";	
	private LandingPageAction lpa;
	private LoginPageAction la;
	private DataTable tm;
	private StandardProductPageAction sppa;
	private YourCartPageAction ycpa;
	private CheckOutInformationPageAction coipa;
	private OverViewCheckOutPageAction ovcopa;
	private CheckOutCompletPageAction cocpa;

	public WebDriver getDriver() {
		return driver;
	}

	@Parameters({"URL","tableName"})
	@BeforeClass
	public void start(String URL,String tableName) throws InterruptedException, UnknownHostException {
		Reporter.log("=====Browser Session Started=====", true);
		System.setProperty("webdriver.chrome.driver",driverPath);
		driver = new ChromeDriver();  
		driver.navigate().to(URL);
		driver.manage().window().maximize();
		Reporter.log("=====Application Started=====", true);
		tm =  DataTable.getMongoDBApplicationInstance();
		tm.getTable(tableName);
		ycpa  = new YourCartPageAction(getDriver());
		ovcopa =  new OverViewCheckOutPageAction(getDriver());
		la = new LoginPageAction(getDriver(),tableName);
	}

	@Test(groups = "Trade Desk Landing Page",
			description = "Verify that the Trade Desk Landing Page has loaded by 'URL'",
			priority = 0)
	public void verifyLiveVlientPageLoadedByURL() throws UnknownHostException {
		lpa = new LandingPageAction(getDriver());
		Assert.assertTrue(lpa.verifyPageByUrl(), "");
	}

	@BeforeGroups(groups = "Login Without Credatials",dependsOnGroups = "Trade Desk Landing Page")
	public void login() throws UnknownHostException {
		la.SwalabsLogInButton();
	}

	@Test(groups = "Login Without Credatials",dependsOnGroups = "Trade Desk Landing Page")
	public void verifyNoUserNameErrorMessage() throws UnknownHostException, InterruptedException {
		Thread.sleep(2000);
		Assert.assertTrue(la.isSwagLabsNoUserNameErrorCorrect());
	}

	@BeforeGroups(groups = "Login With Username Only",dependsOnGroups = "Login Without Credatials")
	public void EnterUsernameAndSubmit() throws UnknownHostException {
		la.SwagLabsUserName();
		la.SwalabsLogInButton();
	}

	@Test(groups = "Login With Username Only",dependsOnGroups = "Login Without Credatials",
			priority = 0)
	public void verifyMissingPassewordErrorMessage() {
		Assert.assertTrue(la.isSwagLabsIncorrectPasswordErrorCorrect(), "Failed to verify ");
	}

	@BeforeGroups(groups = "Login with Locked user",dependsOnGroups = "Login With Username Only")
	public void LoginWIthLockedUswer() {
		la.LoginWithLockedUser();
		la.SwalabsLogInButton();
	}

	@Test(groups = "Login with Locked user",dependsOnGroups = "Login With Username Only" , priority = 0)
	public void verifySwagLabsUserNameWithLockedUsername() {
		Assert.assertTrue(la.isSwagLabsUserNameWithLockedUser(),"");
	}

	@Test(groups = "Login with Locked user",dependsOnGroups = "Login With Username Only" , priority = 1)
	public void verifyPassword() {
		Assert.assertTrue(la.isSwagLabsPasswordCorrect(),"");
	}

	@Test(groups = "Login with Locked user",dependsOnGroups = "Login With Username Only", priority = 2)
	public void verifyLockedUserErrorMessage() {
		Assert.assertTrue(la.isSwagLabsLockedUserNameCorrect(),"");
	}

	@BeforeGroups(groups = "Login with standard user",dependsOnGroups = "Login with Locked user")
	public void standardUserLogin() {
		la.loginWithStandardUser();
		la.SwalabsLogInButton();
		sppa = new StandardProductPageAction(getDriver());
	}

	@Test(groups = "Login with standard user",
			dependsOnGroups = "Login with Locked user", 
			priority = 0)
	public void verifyPageLoadedbyUrl() {
		Assert.assertTrue(lpa.verifyProductByPageByUrl(),"");
	}

	@Test(groups = "Login with standard user",
			dependsOnGroups = "Login with Locked user", 
			priority = 1)
	public void verifyPageLoadedbyTitle() {
		Assert.assertTrue(lpa.verifyProductPageByPageTitle(),"");
	}

	@Test(groups = "Login with standard user",
			dependsOnGroups = "Login with Locked user", 
			priority = 2) 
	public void verifysourceLabsBackbackText() {
		Assert.assertTrue(sppa.verifyListOfProductNames(tm.getFieldValue("SOUCE_LABS_BACKBACK"),sppa.getIterationNumber()));
	}

	@Test(groups = "Login with standard user",
			dependsOnGroups = "Login with Locked user", 
			priority = 3)  
	public void verifysourceLabsBikeLightText() {
		Assert.assertTrue(sppa.verifyListOfProductNames(tm.getFieldValue("SOUCE_LABS_BIKE_LIGHT"),sppa.getIterationNumber()));
	}

	@Test(groups = "Login with standard user",
			dependsOnGroups = "Login with Locked user", 
			priority = 4)  

	public void verifySourceLabsBoltTSshirtText() {
		Assert.assertTrue(sppa.verifyListOfProductNames(tm.getFieldValue("SOUCE_LABS_BOLT_T-SHIRT"),sppa.getIterationNumber()));
	}

	@Test(groups = "Login with standard user",
			dependsOnGroups = "Login with Locked user", 
			priority = 5) 
	public void verifySouceLabsFleeceJacketsText() {
		Assert.assertTrue(sppa.verifyListOfProductNames(tm.getFieldValue("SOUCE_LABS_FLEECE_JACKET"),sppa.getIterationNumber()));
	}

	@Test(groups = "Login with standard user",
			dependsOnGroups = "Login with Locked user", 
			priority = 6) 
	public void verifySourceLabsOniesieText() {
		Assert.assertTrue(sppa.verifyListOfProductNames(tm.getFieldValue("SOUCE_LABS_ONESIE"),sppa.getIterationNumber()));
	}

	@Test(groups = "Login with standard user",
			dependsOnGroups = "Login with Locked user", 
			priority = 7) 
	public void verifyTestAllTheThisngText() {
		Assert.assertTrue(sppa.verifyListOfProductNames(tm.getFieldValue("TEST_ALL_THE_THINGS_T-SHIRTS(RED)"),sppa.getIterationNumber()));
	}

	@BeforeGroups(groups = "Checking Out an Order",dependsOnGroups = "Login with standard user")
	public void CheckingOutanOrder() {
		sppa.ClickAddToCartButton();
		sppa.ClickShoppingCartButton();
	}

	@Test(groups = "Checking Out an Order",
			dependsOnGroups = "Login with standard user", 
			priority = 0) 
	public void verifyCheckOutOrderPage() {
		Assert.assertTrue(ycpa.verifyProductPageByTitle());
	}

	@BeforeGroups(groups = "Compliting An Order",dependsOnGroups = "Checking Out an Order")
	public void checkingOutAnout() throws InterruptedException, UnknownHostException {
		ycpa.ClickCheckOutButton();

	}

	@Test(groups = "Compliting An Order",
			dependsOnGroups = "Checking Out an Order", 
			priority = 0) 
	public void verifyCheckOutPage() throws InterruptedException {
		Assert.assertTrue(ycpa.verifyCheckOutPageByTitle());
	}

	@BeforeGroups(groups = "User Details page",dependsOnGroups = "Compliting An Order")
	public void setUserInformation() throws InterruptedException, UnknownHostException {
		coipa = new CheckOutInformationPageAction(getDriver());
		coipa.setUserNameDetails();

	}

	@Test(groups = "User Details page",
			dependsOnGroups = "Compliting An Order", 
			priority = 0) 
	public void verifyFirstName() throws InterruptedException {
		Assert.assertTrue(coipa.isFirstNameCorrect());
	}

	@Test(groups = "User Details page",
			dependsOnGroups = "Compliting An Order", 
			priority = 1) 
	public void verifyLastName() throws InterruptedException {
		Assert.assertTrue(coipa.isLastNameCorrect());
	}

	@Test(groups = "User Details page",
			dependsOnGroups = "Compliting An Order", 
			priority = 1) 
	public void verifyzipcode() throws InterruptedException {
		Assert.assertTrue(coipa.isZipCodeCorrect());
	}

	@BeforeGroups(groups = "CheckOut button to procced",dependsOnGroups = "User Details page")
	public void clickContinueCheckOutBtn() throws InterruptedException, UnknownHostException {
		coipa.ClickContinueButton();
	}


	@Test(groups = "CheckOut button to procced",
			dependsOnGroups = "User Details page", 
			priority = 0) 
	public void verifyCheckOutPageByTitle() throws InterruptedException {
		Assert.assertTrue(ovcopa.isCheckOutPageActionTitleVisible());
	}

	@BeforeGroups(groups = "Completing checkout",dependsOnGroups = "CheckOut button to procced")
	public void beforeComplitingCheckoutPage() throws InterruptedException, UnknownHostException {
		ovcopa.ClickCheckOutFinishButton();
		cocpa =  new CheckOutCompletPageAction(getDriver());
	}

	@Test(groups = "Completing checkout",
			dependsOnGroups = "CheckOut button to procced", 
			priority = 0) 
	public void verifyCheckOutCompletePageByTitle() throws InterruptedException {
		Assert.assertTrue(cocpa.isCheckOutPageActionTitleVisible());
	}

	@Test(groups = "Completing checkout",
			dependsOnGroups = "CheckOut button to procced",  
			priority = 0) 
	public void verifyCheckOutCompletePageByThankYouText() throws InterruptedException {
		Assert.assertTrue(cocpa.isCheckOutPageThankYouTextVisible());
	}

	@AfterGroups(groups = "Completing checkout",dependsOnGroups = "CheckOut button to procced")
	public void closeBrowser() throws InterruptedException {
		cocpa.ClickBackHomeButton();
		sppa.ClickMenuButton();
		sppa.LogoutButton();
	}

	@BeforeGroups(groups = "Problem User Login",dependsOnGroups = "Completing checkout")
	public void beforeLoginWithProblemUser() throws InterruptedException, UnknownHostException {
		la.LoginWithProbleUserNameAndPassword();
	}

	@Test(groups = "Problem User Login",
			dependsOnGroups = "Completing checkout",  
			priority = 0) 
	public void verifyTheProblemUserName() throws InterruptedException {
		Assert.assertTrue(la.isVerifyProblemUserCorrect());
	}

	@BeforeGroups(groups = "Checkout Problem user Item",dependsOnGroups = "Problem User Login")
	public void CheckOutProblemUser() {
		la.SwalabsLogInButton();
		sppa.ClickAddToCartButton();
		sppa.ClickShoppingCartButton();
	}


	@Test(groups = "Checkout Problem user Item",
			dependsOnGroups = "Problem User Login", 
			priority = 0) 
	public void verifyProblemUserTitle() {
		Assert.assertTrue(ycpa.verifyProductPageByTitle());
	}


	@BeforeGroups(groups = "Problem User Check Item",dependsOnGroups = "Checkout Problem user Item")
	public void checkingOutAnOrderItem() throws InterruptedException, UnknownHostException {
		ycpa.ClickCheckOutButton();

	}

	@Test(groups = "Problem User Check Item",
			dependsOnGroups = "Checkout Problem user Item", 
			priority = 0) 
	public void verifyCheckOutProblemUserPage() throws InterruptedException {
		Assert.assertTrue(ycpa.verifyCheckOutProblemUserPageByTitle());
	}

	@BeforeGroups(groups = "Setting user credentials for checking out Item",dependsOnGroups = "Problem User Check Item")
	public void settingUserCredentials() throws InterruptedException, UnknownHostException {
		coipa.setFirstName();

	}

	@Test(groups = "Setting user credentials for checking out Item"
			,dependsOnGroups = "Problem User Check Item", 
			priority = 0) 
	public void verifyCheckOutFirstName() throws InterruptedException {
		Assert.assertTrue(coipa.isFirstNameCorrect());
	}

	@BeforeGroups(groups = "Verify last Name error message",dependsOnGroups = "Setting user credentials for checking out Item")
	public void verifyLastNameUserErrorMessage() throws InterruptedException, UnknownHostException {
		coipa.ClickContinueButton();

	}

	@Test(groups = "Verify last Name error message",
			dependsOnGroups = "Setting user credentials for checking out Item",
			priority = 0) 
	public void verifyLastNameRequiredMessage() throws InterruptedException {
		Assert.assertTrue(coipa.verifyLastNameRequiredErrorMessage());
	}
	
	@AfterGroups(groups = "Verify last Name error message",dependsOnGroups = "Setting user credentials for checking out Item")
	public void closeTheBrowserAfterTests() throws InterruptedException, UnknownHostException {
		driver.close();
	}
	
}

