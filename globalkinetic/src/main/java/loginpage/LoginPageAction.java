package loginpage;

import java.net.UnknownHostException;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import mongodb.DataTable;


public class LoginPageAction extends LoginPage {

	private WebDriver driver;
	private DataTable tm;

	public LoginPageAction(WebDriver driver) throws UnknownHostException {
		super(driver);
		this.driver = driver;
	}

	public LoginPageAction(WebDriver driver,String tableName) throws UnknownHostException {
		this(driver);
		tm = DataTable.getMongoDBApplicationInstance();
		tm.getTable(tableName);
	}

	public LoginPageAction SwagLabsUserName() {
		getSwagLabsUserName().clear();
		getSwagLabsUserName().sendKeys(tm.getFieldValue("STANDARD_USER"));
		return this;
	}
	
	public boolean isSwagLabsUserNameCorrect() {
		return this.getSwagLabsUserName().getText().equalsIgnoreCase(tm.getFieldValue("STANDARD_USER"));
	}

	public LoginPageAction SwagLabsUserPassword() {
		getSwagLabsUserPassword().clear();
		getSwagLabsUserPassword().sendKeys(tm.getFieldValue("PASSWORD"));
		return this;
	}
	
	public boolean isSwagLabsPasswordCorrect() {
		return this.getSwagLabsUserPassword().getAttribute("value").equalsIgnoreCase(tm.getFieldValue("PASSWORD"));
	}
	
	public LoginPageAction SwagLabsUserNameWithLockedUser() {
		getSwagLabsUserName().clear();
		getSwagLabsUserName().sendKeys(tm.getFieldValue("LOCKED_USER"));
		return this;
	}
	
	public boolean isSwagLabsUserNameWithLockedUser() {
		return this.getSwagLabsUserName().getAttribute("value").equalsIgnoreCase(tm.getFieldValue("LOCKED_USER"));
	}
	
	public LoginPageAction loginWithStandardUser() {
		SwagLabsUserName();
		SwagLabsUserPassword();
		return this;
	}
	
	public boolean isSwagLabsLockedUserNameCorrect() {
		return this.getSwagLabsLockedUserErrorMessage().getText().equalsIgnoreCase(tm.getFieldValue("LOCKED_USER_ERROR_MESSAGE"));
	}
	
	public boolean isSwagLabsNoUserNameErrorCorrect() {
		System.out.print(getSwagLabsLockedUserErrorMessage().getText() + " = "  +  tm.getFieldValue("NO_USERNAME_ERROR_MESSAGE"));
		return this.getSwagLabsLockedUserErrorMessage().getText().contains(tm.getFieldValue("NO_USERNAME_ERROR_MESSAGE"));
	}
	
	public boolean isSwagLabsIncorrectPasswordErrorCorrect() {
		return this.getSwagLabsLockedUserErrorMessage().getText().equalsIgnoreCase(tm.getFieldValue("PASSWORD_REQUIRED_ERROR_MESSAGE"));
	}
	
	public boolean isSwagLabsIncorrectUsernaneAndPasswodCorrect() {
		System.out.print(getSwagLabsLockedUserErrorMessage().getText() + " = "  +  tm.getFieldValue("INCORRECT_USER_AND_PASSWORD_ERROR_MESSSAGE"));
		return this.getSwagLabsLockedUserErrorMessage().getText().equalsIgnoreCase(tm.getFieldValue("INCORRECT_USER_AND_PASSWORD_ERROR_MESSSAGE"));
	}
	
	public LoginPageAction SwalabsLogInButton() {
		System.out.print("print me");
		getSwagLabsUserLoginButton().click();
		return this;
	}
	
	public LoginPageAction LoginWithLockedUser() {
		SwagLabsUserNameWithLockedUser();
		SwagLabsUserPassword();
		return this;
	}
	
	/**
	 * login with problem user
	 * @return
	 */
	public LoginPageAction LoginWithProblemUserName() {
		getSwagLabsUserName().clear();
		getSwagLabsUserName().sendKeys(tm.getFieldValue("PROBLEM_USER"));
		return this;
	}
	
	/**
	 * 
	 * @return
	 */
	public LoginPageAction LoginWithProbleUserNameAndPassword() {
		LoginWithProblemUserName();
		SwagLabsUserPassword();
		return this;
	}
	
	public boolean isVerifyProblemUserCorrect() {
		return getSwagLabsUserName().getAttribute("value").equalsIgnoreCase(tm.getFieldValue("PROBLEM_USER"));
	}
	
	
	
	
}
