package loginpage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
	
	WebDriver driver;

	protected LoginPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver,this);
		
	}

	@FindBy(id = "user-name")
	private WebElement SwagLabsUserName;
	protected WebElement getSwagLabsUserName() {
		return this.SwagLabsUserName;
	}

	@FindBy(id = "password")
	private WebElement SwagLabsUserPassword;
	protected WebElement getSwagLabsUserPassword() {
		return this.SwagLabsUserPassword;
	}
	
	
	@FindBy(css = "input[type='submit']")
	private WebElement SwagLabsUserLoginButton;
	protected WebElement getSwagLabsUserLoginButton() {
		return this.SwagLabsUserLoginButton;
	}
	
	@FindBy(css = "button[class='error-button']")
	@CacheLookup
	private WebElement SwagLabsErrorPasswordButton;
	protected WebElement getSwagLabsErrorPasswordButton() {
		return this.SwagLabsErrorPasswordButton;
	}
	
	@FindBy(xpath = "//*[@id=\"login_button_container\"]/div/form/div[3]")
	private WebElement SwagLabsLockedUserErrorMessage;
	protected WebElement getSwagLabsLockedUserErrorMessage() {
		return this.SwagLabsLockedUserErrorMessage;
	}
	
	
	
	
	

	
}
