package landingPage;

import java.net.UnknownHostException;

import org.openqa.selenium.WebDriver;

import mongodb.DataTable;

public class LandingPageAction extends LandingPage {

	private WebDriver driver;
	private DataTable tm;

	public LandingPageAction(WebDriver driver) throws UnknownHostException {
		super(driver);
		this.driver = driver;
		tm =  DataTable.getMongoDBApplicationInstance();
	}

	public boolean verifyByPageLogo() {
		return getSwagLabsLogo().getAttribute("value").equalsIgnoreCase("login_logo");
	}
	
	public boolean verifyPageByUrl() {
		return driver.getCurrentUrl().equalsIgnoreCase(tm.getFieldValue("SOURCEMO_URL"));
				
	}
	
	public boolean verifyProductPageByPageTitle() {
		return getSwagLabsProductPageTitle().getText().equalsIgnoreCase(tm.getFieldValue("PRODUCT_PAGE_TITLE"));
	}
	
	public boolean verifyProductByPageByUrl() {
		return driver.getCurrentUrl().equalsIgnoreCase(tm.getFieldValue("PRODUCT_PAGE_URL"));
				
	}
	
	

}
