package landingPage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class LandingPage {
	
	WebDriver driver;

	public LandingPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(css = "div[id='login-button']")
	@CacheLookup
	private WebElement SwagLabsAcceptedNames;
	protected WebElement getSwagLabsAcceptedNames() {
		return this.SwagLabsAcceptedNames;
	}
	
	@FindBy(css = "span[class='login_logo']")
	@CacheLookup
	private WebElement SwagLabsLogo;
	protected WebElement getSwagLabsLogo() {
		return this.SwagLabsLogo;
	}
	
	@FindBy(css = "span[class='title']")
	@CacheLookup
	private WebElement SwagLabsProductPageTitle;
	protected WebElement getSwagLabsProductPageTitle() {
		return this.SwagLabsProductPageTitle;
	}
}
