package overviewcheckoutpage;

import java.net.UnknownHostException;

import org.openqa.selenium.WebDriver;

import mongodb.DataTable;

public class OverViewCheckOutPageAction extends OverViewCheckOutPage {

	private WebDriver driver;
	private DataTable tm;
	
	public OverViewCheckOutPageAction(WebDriver driver) throws UnknownHostException {
		super(driver);
		this.driver = driver;
		tm = DataTable.getMongoDBApplicationInstance();
	}
	
	public boolean isCheckOutPageActionTitleVisible() {
		return getSwagLabsOverViewPageTitle().getText().equalsIgnoreCase(tm.getFieldValue("CHECKOUT_OVERVIEW_TITLE"));
	}
	
	public OverViewCheckOutPageAction ClickCheckOutFinishButton() {
		getSwagLabsOverViewFinishbtn().click();
		return this;
	}
	
}
