package overviewcheckoutpage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class OverViewCheckOutPage {

	public OverViewCheckOutPage(WebDriver driver) {
		super();
		PageFactory.initElements(driver, this);

	}

	@FindBy(css = "span[class='title']")
	@CacheLookup
	private WebElement swagLabsOverViewPageTitle;
	protected WebElement getSwagLabsOverViewPageTitle() {
		return this.swagLabsOverViewPageTitle;
	}
	
	@FindBy(css = "button[class='btn btn_action btn_medium cart_button']")
	@CacheLookup
	private WebElement swagLabsOverViewFinishbtn;
	protected WebElement getSwagLabsOverViewFinishbtn() {
		return this.swagLabsOverViewFinishbtn;
	}
	

}
