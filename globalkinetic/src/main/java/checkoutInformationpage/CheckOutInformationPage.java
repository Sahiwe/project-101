package checkoutInformationpage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CheckOutInformationPage {
	
	public CheckOutInformationPage(WebDriver driver) {
		super();
		PageFactory.initElements(driver, this);
		
	}
	
	@FindBy(css = "input[placeholder='First Name']")
	private WebElement firstName;
	protected WebElement getFirstName() {
		return this.firstName;
	}
	
	@FindBy(css = "input[placeholder='Last Name']")
	private WebElement lastName;
	protected WebElement getLastName() {
		return this.lastName;
	}
	
	@FindBy(css = "input[placeholder='Zip/Postal Code']")
	private WebElement zipCode;
	protected WebElement getZipCode() {
		return this.zipCode;
	}
	
	@FindBy(css = "input[type='submit']")
	private WebElement continueButton;
	protected WebElement getContinueButton() {
		return this.continueButton;
	}
	
	@FindBy(css = "h3[data-test='error']")
	private WebElement lastNameRequiredErrorMessage;
	protected WebElement getLastNameRequiredErrorMessage() {
		return this.lastNameRequiredErrorMessage;
	}
}


