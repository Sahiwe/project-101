package checkoutInformationpage;

import java.net.UnknownHostException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import mongodb.DataTable;

public class CheckOutInformationPageAction extends CheckOutInformationPage{

	private WebDriver driver;
	private DataTable tm;

	public CheckOutInformationPageAction(WebDriver driver) throws UnknownHostException {
		super(driver);
		this.driver = driver;
		tm = DataTable.getMongoDBApplicationInstance();
	}


	public CheckOutInformationPageAction setFirstName() {
		getFirstName().sendKeys(tm.getFieldValue("FIRST_NAME"));
		return this;
	}

	public boolean isFirstNameCorrect() {
		return getFirstName().getAttribute("value").equalsIgnoreCase(tm.getFieldValue("FIRST_NAME"));
	}

	public CheckOutInformationPageAction setLastName() {
		getLastName().sendKeys(tm.getFieldValue("LAST_NAME"));
		return this;
	}

	public boolean isLastNameCorrect() {
		return getLastName().getAttribute("value").equalsIgnoreCase(tm.getFieldValue("LAST_NAME"));
	}
	/**
	 * 
	 * @return
	 */
	public CheckOutInformationPageAction setZipCode() {
		getZipCode().sendKeys(tm.getFieldValue("ZIP_CODE"));
		return this;
	}

	public boolean isZipCodeCorrect() {
		return getZipCode().getAttribute("value").equalsIgnoreCase(tm.getFieldValue("ZIP_CODE"));
	}

	public CheckOutInformationPageAction ClickContinueButton() {
		getContinueButton().click();
		return this;
	}
	
	public CheckOutInformationPageAction setUserNameDetails() {
		setFirstName();
		setLastName();
		setZipCode();
		return this;
	}
	
	public boolean verifyLastNameRequiredErrorMessage() {
		return getLastNameRequiredErrorMessage().getText().equalsIgnoreCase(tm.getFieldValue("LAST_NAME_REQUIRED_ERROR_MESSAGE"));
	}


}
