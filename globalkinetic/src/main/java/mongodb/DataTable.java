package mongodb;

import java.io.FileReader;
import java.net.UnknownHostException;

import org.testng.Reporter;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mongodb.*;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;

import de.flapdoodle.embed.mongo.distribution.Version;
import de.flapdoodle.embed.mongo.tests.MongodForTestsFactory;


public class DataTable {

	private DB db = null;
	DBCursor cursor = null;
	private BasicDBObject query = new BasicDBObject();
	private DBCollection collection = null;
	private final String localhost = "localhost";
	private final String dbName = "tt";
	private MongoClient mongo = null;
	private static DataTable mongoInstance = null;
	private final static String projectPath = System.getProperty("user.dir");
	private final String TESTING_DATA =  projectPath +"\\src\\test\\resource\\testing_data.json";

	private MongodForTestsFactory factory;

	private DataTable()  {
		try {
			factory = MongodForTestsFactory.with(Version.Main.PRODUCTION);
			MongoClient m = factory.newMongo();
			//Reporter.log(connectionstrings);
			db = m.getDB(dbName);
			MongoCredential credential = MongoCredential.createCredential("journaldev", dbName, "journaldev".toCharArray());
			//Reporter.log(credential);
		} catch(Exception e) {
			Reporter.log("Error connecting to mongo");
		}
	}
	

	public static DataTable getMongoDBApplicationInstance() throws UnknownHostException   {
		if(mongoInstance == null) {
			mongoInstance = new DataTable();
		}
		return mongoInstance;
	}

	public String setFirstCharToUpper(String tosetString) {
		return Character.toUpperCase(tosetString.charAt(0)) + tosetString.substring(1); 
	}

	public static boolean isCollectionExists(DB db, String collectionName) {
		DBCollection table = db.getCollection(collectionName);
		return (table.count()>0)?true:false;
	}
	

	public void writeJsonToMongo(String jsonOb) {
		Object tableOject = com.mongodb.util.JSON.parse(jsonOb);
		DBObject dbObj = (DBObject)tableOject;
		collection.insert(dbObj);	
	}

	public static JsonObject readJsonFile(String filename) {
		JsonParser parser = new JsonParser();
		JsonObject jsonObject = null;
		try {
			JsonElement jsonElem = parser.parse(new FileReader(filename));
			jsonObject = jsonElem.getAsJsonObject(); 
		}catch (Exception e) {
			Reporter.log(e.getMessage());
		}
		return jsonObject;
	}

	public DataTable getTable(String DbName) {
		if (!isCollectionExists(db,setFirstCharToUpper(DbName))) {
			Reporter.log("=====created succsefully=====", true);
			collection = db.createCollection(setFirstCharToUpper(DbName),null);
			writeJsonToMongo(readJsonFile(TESTING_DATA).get(DbName).toString());
			Reporter.log("=====created succsefully=====", true);
		} else{
			collection = db.getCollection(setFirstCharToUpper(DbName));
			Reporter.log("=====selected succsefully=====", true);
		}
		return this;
	}
	
	public String getFieldValue(String key) {
		String value = null;
		cursor = collection.find(query);
		while (cursor.hasNext()) {
			value=cursor.next().get(key).toString();
		}
		return value;
	}

}
