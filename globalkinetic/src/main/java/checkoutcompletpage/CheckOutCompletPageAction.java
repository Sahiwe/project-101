package checkoutcompletpage;

import java.net.UnknownHostException;

import org.openqa.selenium.WebDriver;

import mongodb.DataTable;

public class CheckOutCompletPageAction extends CheckOutCompletPage{

	private WebDriver driver;
	private DataTable tm;
	
	public CheckOutCompletPageAction(WebDriver driver) throws UnknownHostException {
		super(driver);
		this.driver= driver;
		tm = DataTable.getMongoDBApplicationInstance();
	}
	
	public boolean isCheckOutPageActionTitleVisible() {
		return getSwagLabsCheckOutCompletePageTitle().getText().equalsIgnoreCase(tm.getFieldValue("CHECKOUT_COMPLETE_TITLE"));
	}
	
	public boolean isCheckOutPageThankYouTextVisible() {
		return getSwagLabsCheckoutComplete().getText().equalsIgnoreCase(tm.getFieldValue("CHECKOUT_COMPLETE_THANK_YOU_MESSAGE"));
	}
	
	public CheckOutCompletPageAction ClickBackHomeButton() {
		getSwagLabsCheckoutOrderBackToHomeBtn().click();
		return this;
	}
}
