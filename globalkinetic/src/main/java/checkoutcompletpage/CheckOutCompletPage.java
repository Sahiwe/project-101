package checkoutcompletpage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CheckOutCompletPage {

	public CheckOutCompletPage(WebDriver driver) {
		super();
		PageFactory.initElements(driver, this);
		
	}
	
	@FindBy(css = "span[class='title']")
	@CacheLookup
	private WebElement swagLabsCheckOutCompletePageTitle;
	protected WebElement getSwagLabsCheckOutCompletePageTitle() {
		return this.swagLabsCheckOutCompletePageTitle;
	}
	
	@FindBy(css = "#checkout_complete_container > h2")
	@CacheLookup
	private WebElement swagLabsCheckoutComplete;
	protected WebElement getSwagLabsCheckoutComplete() {
		return this.swagLabsCheckoutComplete;
	}
	
	@FindBy(css = "button[id='back-to-products']")
	@CacheLookup
	private WebElement swagLabsCheckoutOrderBackToHomeBtn;
	protected WebElement getSwagLabsCheckoutOrderBackToHomeBtn() {
		return this.swagLabsCheckoutOrderBackToHomeBtn;
	}
	
}
