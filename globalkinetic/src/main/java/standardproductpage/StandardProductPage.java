package standardproductpage;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class StandardProductPage {
	
	public StandardProductPage(WebDriver driver) {
		super();
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(css = "div[class='inventory_item_name']")
	private List<WebElement> productNames;
	protected List<WebElement> getProductNames() {
		return this.productNames;
	}
	
	@FindBy(css = "button[id='add-to-cart-sauce-labs-backpack']")
	private WebElement addToCartButton;
	protected WebElement getAddToCartButton() {
		return this.addToCartButton;
	}
	
	
	@FindBy(css = "a[class='shopping_cart_link']")
	private WebElement shoppingCartButton;
	protected WebElement getshoppingCartButton() {
		return this.shoppingCartButton;
	}
	
	@FindBy(css = "div[class='bm-burger-button']")
	private WebElement menuButton;
	protected WebElement getMenuButton() {
		return this.menuButton;
	}
	
	@FindBy(css = "a[id='logout_sidebar_link']")
	private WebElement logoutButton;
	protected WebElement getLogoutButton() {
		return this.logoutButton;
	}
	
	
	
	
	
	

}
