package standardproductpage;

import org.openqa.selenium.WebDriver;

public class StandardProductPageAction extends StandardProductPage {

	private WebDriver driver;
	private int interationNumber = 0;
	
	public StandardProductPageAction(WebDriver driver) {
		super(driver);
		this.driver = driver;
	}
	
	public boolean verifyListOfProductNames(String expected , int iterartion) {
		System.out.print("Number of Error messages :" + getProductNames().size());
		int i;
		System.out.print("INTERATION NUMBER  : "+iterartion);
		for(i  = iterartion ; i < getProductNames().size(); i++) {
			iterartion = iterartion +1;
			setInteration(iterartion);
			System.out.print("Error messages value :" +  getProductNames().get(i).getText() + " = " + expected);
			return getProductNames().get(i).getText().equalsIgnoreCase(expected);
		}
		return false;
	}

	private void setInteration(int inter) {
		interationNumber = inter;
	}
	
	public int getIterationNumber() {
		return interationNumber;
	}
	/**
	 * 
	 * @return
	 */
	public StandardProductPageAction ClickAddToCartButton() {
		getAddToCartButton().click();
		return this;
	}
	/**
	 * 
	 * @return
	 */
	public StandardProductPageAction ClickShoppingCartButton() {
		getshoppingCartButton().click();
		return this;
	}
	/**
	 * 
	 * @return
	 */
	public StandardProductPageAction ClickMenuButton() {
		getMenuButton().click();
		return this;
	}
	
	public StandardProductPageAction LogoutButton() {
		getLogoutButton().click();
		return this;
	}
	
	
	
	
	
}
