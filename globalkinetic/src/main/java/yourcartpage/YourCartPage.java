package yourcartpage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class YourCartPage {
	
	public YourCartPage(WebDriver driver) {
		super();
		PageFactory.initElements(driver, this);
		
	}
	
	@FindBy(css = "span[class='title']")
	private WebElement yourCartTitle;
	protected WebElement getYourCartTitle() {
		return this.yourCartTitle;
	}
	
	@FindBy(css = "button[data-test='checkout']")
	private WebElement checkoutbutton;
	protected WebElement getCheckoutbutton() {
		return this.checkoutbutton;
	}
	
	
}
