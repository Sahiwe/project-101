package yourcartpage;

import java.net.UnknownHostException;

import org.openqa.selenium.WebDriver;
import mongodb.DataTable;

public class YourCartPageAction extends YourCartPage {

	private WebDriver driver;
	private DataTable tm;

	public YourCartPageAction(WebDriver driver) throws UnknownHostException {
		super(driver);
		this.driver = driver;
		tm =  DataTable.getMongoDBApplicationInstance();
	}

	public boolean verifyProductPageByTitle() {
		return getYourCartTitle().getText().equalsIgnoreCase(tm.getFieldValue("YOUR_CART_TITLE"));
	}
	/**
	 * 
	 * @return
	 */
	public YourCartPageAction ClickCheckOutButton() {
		getCheckoutbutton().click();
		return this;
	} 
	/**
	 * 
	 * @return
	 */
	public boolean verifyCheckOutPageByTitle() {
		return getYourCartTitle().getText().equalsIgnoreCase(tm.getFieldValue("CHECKOUT_YOUR_INFORMATION"));
	}
	
	public boolean verifyCheckOutProblemUserPageByTitle() {
		return getYourCartTitle().getText().equalsIgnoreCase(tm.getFieldValue("CHECKOUT_ITEM_WITH_PROBLEM_USER"));
	}
	
	

}
